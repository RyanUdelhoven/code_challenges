# Code Challenges
Just doing some stuff to keep me sharp.

## Start Up
1. In the index.html uncomment the challenges you want to see perform, comment the ones you don't
2. Open index.html in your browser
3. Open the console.log
4. Observe

## Challenge Sourcing
1. HackerRank.com
2. LeetCode.com
3. Various SubReddits
4. Anything I make up that I think is cool